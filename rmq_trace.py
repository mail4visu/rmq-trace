#!/usr/bin/env python

from __future__ import print_function
import getopt
import json
import pika
import sys


def usage():
  print('Usage: %s OPTIONS' % argv[0])
  print('\nOPTIONS:')
  print(' [-u] (--user)      user with permissions on amq.rabbitmq.trace exchange')
  print(' [-p] (--password)  password for trace user')
  print(' [-t] (--target)    IP RabbitMQ is bound to')
  print(' [-h] (--help)      print this message')


def callback(ch, method, properties, body):
  print("[*] routing key: %s\n    node: %s\n    headers: %s\n    body:\n%s\n" % (
         method.routing_key, 
         properties.headers["node"],
         properties.headers["routing_keys"], 
         json.loads(body),
         ))
   

def main():
  try:
    opts, args = getopt.getopt(sys.argv[1:], "u:p:t:h", ["user=", "password=", "target=", "help"])
  except getopt.GetoptError, err:
    print(str(err))
    usage()
    return 1
  for opt, arg in opts:
    if opt in ("-u", "--user"):
      user = arg
    elif opt in ("-p", "--password"):
      password = arg
    elif opt in ("-t", "--target"):
      target = arg
    elif opt in ("-h", "--help"):
      usage()
      return 0
    else:
      assert False, '<*> unhandled option'
      return 1

  # set defaults if unspecified
  try:
    user
  except:
    user = 'guest'
    print("> using 'guest' as the username")

  try:
    password
  except:
    print("> using 'guest' as the password")
    password = 'guest'

  try:
    target
  except:
    target = 'localhost'
    print("> connecting to 'localhost'")

  # connect to RabbitMQ
  credentials = pika.PlainCredentials(user, password)
  connection = pika.BlockingConnection(pika.ConnectionParameters(
            target,
            5672,
            '/',
            credentials))
  channel = connection.channel()
  queue_name = "firehose"
  result = channel.queue_declare(queue=queue_name, exclusive=False)
        
  channel.queue_bind(exchange='amq.rabbitmq.trace',
                     queue=queue_name,
                     routing_key="#")
    
  channel.basic_consume(callback,
                        queue=queue_name,
                        no_ack=True)
    
  channel.start_consuming()

if __name__ == "__main__":
  rc = main()
  exit(rc)

